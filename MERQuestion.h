//
//  MERQuestion.h
//  Labb2
//
//  Created by ITHS on 2016-02-04.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MERQuestion : NSObject
@property (nonatomic, strong) NSString *question;
@property (nonatomic, strong) NSString *answer1;
@property (nonatomic, strong) NSString *answer2;
@property (nonatomic, strong) NSString *answer3;
@property (nonatomic, strong) NSString *answer4;
@property (nonatomic, strong) NSString *correctAnswer;
@end
