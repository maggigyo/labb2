//
//  MERQuestionManager.m
//  Labb2
//
//  Created by ITHS on 2016-02-04.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "MERQuestionManager.h"
#import "MERQuestion.h"

@implementation MERQuestionManager
-(instancetype) init
{
    self = [super init];
    if(self)
    {
        self.questions = [[NSArray alloc]init];
        MERQuestion *q = [[MERQuestion alloc] init];
        q.question = @"What year was World of Warcraft released?";
        q.answer1 = @"2001";
        q.answer2 = @"2002";
        q.answer3 = @"2003";
        q.answer4 = @"2004";
        q.correctAnswer = @"2004";
        self.questions =  [self.questions arrayByAddingObject:q];
        
        q = [[MERQuestion alloc] init];
        q.question = @"What year was Halo released for XBox?";
        q.answer1 = @"2003";
        q.answer2 = @"2001";
        q.answer3 = @"2004";
        q.answer4 = @"2000";
        q.correctAnswer = @"2001";
        self.questions =  [self.questions arrayByAddingObject:q];
        
        q = [[MERQuestion alloc] init];
        q.question = @"What year was Super Mario Bros. released?";
        q.answer1 = @"1987";
        q.answer2 = @"1985";
        q.answer3 = @"1983";
        q.answer4 = @"1989";
        q.correctAnswer = @"1985";
        self.questions =  [self.questions arrayByAddingObject:q];
        
        q = [[MERQuestion alloc] init];
        q.question = @"What year was The Legend of Zelda: A Link to the Past released?";
        q.answer1 = @"1997";
        q.answer2 = @"1995";
        q.answer3 = @"1993";
        q.answer4 = @"1991";
        q.correctAnswer = @"1991";
        self.questions =  [self.questions arrayByAddingObject:q];
        
        q = [[MERQuestion alloc] init];
        q.question = @"What year was The Sims released?";
        q.answer1 = @"2000";
        q.answer2 = @"2001";
        q.answer3 = @"2002";
        q.answer4 = @"2003";
        q.correctAnswer = @"2000";
        self.questions =  [self.questions arrayByAddingObject:q];
        
        q = [[MERQuestion alloc] init];
        q.question = @"What year was Half-Life released?";
        q.answer1 = @"1999";
        q.answer2 = @"2000";
        q.answer3 = @"1997";
        q.answer4 = @"1998";
        q.correctAnswer = @"1998";
        self.questions =  [self.questions arrayByAddingObject:q];
        
        q = [[MERQuestion alloc] init];
        q.question = @"What year was Minecraft (Classic) released?";
        q.answer1 = @"2007";
        q.answer2 = @"2003";
        q.answer3 = @"2009";
        q.answer4 = @"2005";
        q.correctAnswer = @"2009";
        self.questions =  [self.questions arrayByAddingObject:q];
        
        q = [[MERQuestion alloc] init];
        q.question = @"What year was ICO first released?";
        q.answer1 = @"2001";
        q.answer2 = @"2003";
        q.answer3 = @"2004";
        q.answer4 = @"2000";
        q.correctAnswer = @"2001";
        self.questions =  [self.questions arrayByAddingObject:q];
        
        q = [[MERQuestion alloc] init];
        q.question = @"What year was Silent Hill released?";
        q.answer1 = @"1996";
        q.answer2 = @"1997";
        q.answer3 = @"1998";
        q.answer4 = @"1999";
        q.correctAnswer = @"1999";
        self.questions =  [self.questions arrayByAddingObject:q];
        
        q = [[MERQuestion alloc] init];
        q.question = @"What year was Wii Fit released?";
        q.answer1 = @"2003";
        q.answer2 = @"2009";
        q.answer3 = @"2007";
        q.answer4 = @"2005";
        q.correctAnswer = @"2007";
        self.questions =  [self.questions arrayByAddingObject:q];
    }
    
    return self;
}

-(void) newGame: (int)numberOfQuestions
{
    self.currentQuestionIndex = 0;
    self.incorrectGuesses = 0;
    self.correctGuesses = 0;
    self.randomQuestions = [[NSArray alloc]init];
    NSSet *usedIndexes = [[NSSet alloc]init];
    while(self.randomQuestions.count < numberOfQuestions)
    {
        int random = arc4random() % [self.questions count];
        if(![usedIndexes containsObject:@(random)])
        {
            usedIndexes = [usedIndexes setByAddingObject:@(random)];
            NSLog(@"Added %@", @(random));
            self.randomQuestions =  [self.randomQuestions arrayByAddingObject: self.questions[random]];

        }
    }
}

-(MERQuestion*) getNextQuestion
{
    if(self.currentQuestionIndex >= self.randomQuestions.count)
    {
        return nil;
    } else {
    return self.randomQuestions[self.currentQuestionIndex];
    }
}

-(BOOL) checkAnswer: (NSString*)answer
{
    MERQuestion *q = self.randomQuestions[self.currentQuestionIndex];
    self.currentQuestionIndex++;//prepare for next question
    if([answer isEqualToString:q.correctAnswer])
    {
        self.correctGuesses++;
        return YES;
    }else{
        self.incorrectGuesses++;
        return NO;
    }
}

@end
