//
//  ViewController.m
//  Labb2
//
//  Created by ITHS on 2016-02-02.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "ViewController.h"
#import "MERQuestion.h"
#import "MERQuestionManager.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutlet UILabel *correctLabel;
@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIButton *button3;
@property (strong, nonatomic) IBOutlet UIButton *button4;
@property (strong, nonatomic) MERQuestionManager *qm;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;
@property (strong, nonatomic) IBOutlet UIButton *nButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.qm = [[MERQuestionManager alloc]init];
    [self.qm newGame:5];
    [self next:self.nextButton];
    
}
                               
-(void)updateItemsInView:(MERQuestion *) q{
    self.nButton.hidden = YES;
    self.nextButton.hidden = NO;
    self.button1.hidden = NO;
    self.button2.hidden = NO;
    self.button3.hidden = NO;
    self.button4.hidden = NO;
    self.correctLabel.text = @"-";
    self.questionLabel.text = q.question;
    [self.button1 setTitle:q.answer1 forState:UIControlStateNormal];
    [self.button2 setTitle:q.answer2 forState:UIControlStateNormal];
    [self.button3 setTitle:q.answer3 forState:UIControlStateNormal];
    [self.button4 setTitle:q.answer4 forState:UIControlStateNormal];
    self.nextButton.enabled = NO;

}

- (IBAction)answerQuestion:(UIButton *)sender {
    BOOL correct = [self.qm checkAnswer: sender.titleLabel.text];
    if(correct)
    {
        self.correctLabel.text = @"Correct!";
    }
    else
    {
        self.correctLabel.text = @"Wrong answer..";
    }
    self.nextButton.enabled = YES;
}


- (IBAction)next:(UIButton *)sender {
    MERQuestion *q = self.qm.getNextQuestion;
    if(q == nil)
    {
        [self end];
        return;
    }
    else
    {
        [self updateItemsInView:q];
    }

}

-(void)end{
    self.correctLabel.text = [NSString stringWithFormat:@"You got %d correct and %d incorrect answers.", self.qm.correctGuesses, self.qm.incorrectGuesses];
    self.questionLabel.text = @"Game Over";
    self.nextButton.hidden = YES;
    self.button1.hidden = YES;
    self.button2.hidden = YES;
    self.button3.hidden = YES;
    self.button4.hidden = YES;
    self.nButton.hidden = NO;
}

- (IBAction)restart:(UIButton *)sender {
    [self.qm newGame:5];
    [self next:self.nextButton];
}



@end
