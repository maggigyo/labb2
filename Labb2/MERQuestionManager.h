//
//  MERQuestionManager.h
//  Labb2
//
//  Created by ITHS on 2016-02-04.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MERQuestion.h"

@interface MERQuestionManager : NSObject
@property (strong, nonatomic)NSArray *questions;
@property (strong, nonatomic)NSArray *randomQuestions;
@property int currentQuestionIndex;
@property int correctGuesses;
@property int incorrectGuesses;

-(instancetype) init;
-(void) newGame: (int)numberOfQuestions;
-(MERQuestion*) getNextQuestion;
-(BOOL) checkAnswer: (NSString*)answer;


@end
